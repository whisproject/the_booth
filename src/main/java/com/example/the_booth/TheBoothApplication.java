package com.example.the_booth;

import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TheBoothApplication {

    public static void main(String[] args) {
        SpringApplication.run(TheBoothApplication.class, args);
    }

}
