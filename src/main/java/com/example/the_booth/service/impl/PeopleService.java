package com.example.the_booth.service.impl;

import com.example.the_booth.dao.LinkDao;
import com.example.the_booth.dao.PeopleDao;
import com.example.the_booth.dao.RentBoothsDao;
import com.example.the_booth.entity.Link;
import com.example.the_booth.entity.People;
import com.example.the_booth.util.MD5Util;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.sound.midi.Soundbank;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 王福坤
 * 2020/8/3 10:17
 */
@Service
public class PeopleService implements com.example.the_booth.service.PeopleService {

    HashMap<Object, Object> reluse =new HashMap<>();

    @Resource
    private PeopleDao peopleDao;

    @Resource
    private LinkDao linkDao;

    @Resource
    private RentBoothsDao rentBoothsDao;

    @Override
    public HashMap<Object,Object> insert(Map<String,People> map) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        System.out.println(map);
        //拿到对象
        People people = map.get("people");

        //这里需要加密，所以需要重新赋加密的密码
        people.setMer_password(MD5Util.getEncryptedPwd(people.getMer_password()));
        //        查询账号是否存在
        People TestExist = peopleDao.TestExists(people.getMer_id());
//        判断账号是否存在
        if(TestExist == null){
//            注册成功
            peopleDao.insert(people);
            reluse.put("status","ok");
        }else {
//            注册失败
            reluse.put("status","error");
        }
        System.out.println(reluse);
        return reluse;
    }

    @Override
    public People TestExists(String merId) {
        return this.peopleDao.TestExists(merId);
    }

    @Override
    public HashMap<Object, Object> login(Map<String, People> map) throws UnsupportedEncodingException, NoSuchAlgorithmException {

        People people = map.get("people");
        people.setMer_password(people.getMer_password());
//        //        查询账号是否存在
        People TestExist = peopleDao.TestExists(people.getMer_id());
        if(TestExist == null){
//           账号不存在
            reluse.put("message","登陆失败,账号不存在");
            reluse.put("status","error");
        }else {
            System.out.println(peopleDao.QueryLogin(people.getMer_id()));
            boolean check=MD5Util.validPassword(people.getMer_password(),peopleDao.QueryLogin(people.getMer_id()));
            System.out.println(check);
//           账号存在
            if(check){
//            登录成功
                reluse.put("status","ok");
                reluse.put("people",peopleDao.TestExists(people.getMer_id()));

                reluse.remove("message");
            }else {
//            登录失败
                reluse.put("message","登陆失败，密码不正确");
                reluse.put("status","error");
            }

        }
        return reluse;
    }

    @Override
    public String QueryLogin(String merId) {
        return this.peopleDao.QueryLogin(merId);
    }

    @Override
    public HashMap<Object, Object> Modify(Map<String,Object> map) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        People people = new People();
        System.out.println(map);
        people.setMer_id((String)map.get("mer_id"));
        people.setMer_password(MD5Util.getEncryptedPwd((String)map.get("mer_password")));
        People TestExist = this.peopleDao.TestExists(people.getMer_id());
        System.out.println(TestExist);
        if (TestExist == null) {
            this.reluse.put("message", "修改失败,账号不存在");
            this.reluse.put("status", "error");
        } else {
            this.peopleDao.Updatapassword(people.getMer_password(), people.getMer_id());
            if (this.peopleDao.returns() > 0) {
                this.reluse.put("status", "ok");
            } else {
                this.reluse.put("message", "修改失败");
            }
        }

        System.out.println(this.reluse);
        return this.reluse;
    }

    @Override
    public int returns() {
        return peopleDao.returns();
    }

    @Override
    public HashMap<Object, Object> infadmin() {
        HashMap<Object, Object> reluse =new HashMap<>();
        List<People> people = peopleDao.infpeople("a");
        reluse.put("people",people);
        return reluse;
    }


    @Override
    public HashMap<Object, Object> infuser() {
        HashMap<Object, Object> reluse =new HashMap<>();
        List<People> people = peopleDao.infpeople("b");
        reluse.put("people",people);
        return reluse;
    }

    @Override
    public Map<String, String> queryName(Integer rb_id) {

        HashMap<String,String> reslt = new HashMap<>();
        String mer_id = linkDao.rbidQueryphone(rb_id);
        if (mer_id ==null){
            reslt.put("status","error");
            reslt.put("message","查不到与该地摊绑定的用户");
        }else {
            String mer_name = peopleDao.queryName(mer_id);
            reslt.put("status","ok");
            reslt.put("mer_name",mer_name);
        }

        return reslt;
    }

    @Override
    public Map<String, String> deleteUser(Map<String,String> map) {
        HashMap<String,String> back = new HashMap<>();
        String mer_id = map.get("mer_id");
        Integer rb_id = linkDao.queryRbid(mer_id);
        if (rb_id==null){
            int status = 0;
            try {
                status = peopleDao.deleteUser(mer_id);
            } catch (Exception e) {
                e.printStackTrace();
                back.put("status","error");
                back.put("message","改管理员名下有地摊信息，无法删除！");
                return back;
            }
            if (status == 1){
                back.put("status","ok");
                back.put("message","删除成功");
            }
            if (status == 0){
                back.put("status","error");
                back.put("message","删除失败");
            }
        }

        if (rb_id!=null){
            Integer rb_status = rentBoothsDao.searchBoothStatus(rb_id);
            System.out.println(rb_status);
            switch (rb_status){
                case 1:
                    back.put("status","error");
                    back.put("message","该用户已租摊位，无法删除！");
                    break;
                case 2:
                    back.put("status","error");
                    back.put("message","该用户已申报摊位，无法删除！");
                    break;
                case 3:
                    back.put("status","error");
                    back.put("message","该用户已申请摊位，无法删除！");
                    break;
            }
        }

        return back;
    }

    public List<People> findUserByNameAndPhone(String name, String phone){
        return peopleDao.findUserByNameAndPhone(name,phone);
    }

}

