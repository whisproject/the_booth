package com.example.the_booth.service.impl;

import com.example.the_booth.dao.LinkDao;
import com.example.the_booth.dao.RentBoothsDao;;
import com.example.the_booth.entity.Link;
import com.example.the_booth.entity.RentBooths;
import com.example.the_booth.entity.RentBooths2;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.rmi.MarshalledObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 王福坤
 * 2020/8/5 16:03
 */
@Service
public class LinkServicce implements com.example.the_booth.service.LinkServicce {
    @Resource
    private LinkDao linkDao;

    @Resource
    private RentBoothsDao rentBoothsDao;

    @Override
    public HashMap<Object, Object> applybooths(Map<String,Object> map) throws ParseException {
        HashMap<Object, Object> reluse =new HashMap<>();


        RentBooths2 renbooths = (RentBooths2) JSONObject.toBean(JSONObject.fromObject(map.get("renbooths")), RentBooths2.class);

        System.out.println(renbooths);


        String introduce =(String) map.get("introduce");
        String industry = (String) map.get("industry");
        String date = (String) map.get("date");

        System.out.println(renbooths + industry + introduce +date);

        Link link = new Link();
        link.setLi_mer_id(renbooths.getRb_adm_id());
        link.setLi_rb_id(renbooths.getRb_id());
        link.setLi_over_time(date);
        link.setLi_begin_time(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()));
        link.setLi_industry(industry);
        link.setLi_introduce(introduce);


        System.out.println(renbooths.getRb_adm_id());
        String queryphone = linkDao.queryphone(renbooths.getRb_adm_id());
        if (queryphone == null){
            //            判断摊位此时状态是否是未租
            if (rentBoothsDao.getrb_state(renbooths.getRb_id())==0) {
//            绑定用户与摊位
                linkDao.applybooths(link);
                //            数据库受影响行数大于一
                if (linkDao.linkreturns()>0) {
                    rentBoothsDao.Updatastate(3,renbooths.getRb_id());

//                申请已提交
                    reluse.put("message","申请已提交");
                    reluse.put("status","ok");

                }else {
//                申请提交失败
                    reluse.put("message","申请提交失败");
                    reluse.put("status","error");
                }
            }else {
//            此摊位已租或以被申请
                reluse.put("message","此摊位已租或以被申请");
                reluse.put("status","error");

            }
        }else {
            //已被申请
            reluse.put("message","您已申请过摊位，请耐心等待审核");
            reluse.put("status","error");
        }


        System.out.println(reluse);

        return reluse;
    }


    @Override
    public int linkreturns() {
        return linkDao.linkreturns();
    }


    /**
     * 通过或拒绝用户申请的摊位  传入申请摊位的id,与状态 1 代表允许,0代表拒绝
     * 同意时同时要在rentboots表修改摊位状态为1(已租)
     * @return
     */
    public boolean isafter(int id,int a_static){

        boolean isafter = linkDao.isafter(id, a_static);

        if (a_static == 1){
            Integer rb_id = linkDao.queryRbid(linkDao.liidQueryphone(id));

            rentBoothsDao.Updatastate(1,rb_id);
        }
        if (a_static == 2){
            Integer rb_id = linkDao.queryRbid(linkDao.liidQueryphone(id));

            rentBoothsDao.Updatastate(0,rb_id);
        }

        return isafter;
    }

}
