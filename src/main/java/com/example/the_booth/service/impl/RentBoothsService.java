package com.example.the_booth.service.impl;

import com.example.the_booth.dao.LinkDao;
import com.example.the_booth.dao.PeopleDao;
import com.example.the_booth.dao.RentBoothsDao;
import com.example.the_booth.entity.ImageConst;
import com.example.the_booth.entity.People;
import com.example.the_booth.entity.RentBooths;
import com.example.the_booth.entity.RentBooths2;
import com.example.the_booth.util.Base64Util;
import com.example.the_booth.vo.LinkSuperVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.reactive.function.client.ReactorNettyHttpClientMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
//import sun.plugin.javascript.navig.Link;

import javax.annotation.Resource;
import java.io.File;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 王福坤
 * 2020/8/5 14:55
 */
@Service
public class RentBoothsService implements com.example.the_booth.service.RentBoothsService {

    @Resource
    private LinkDao linkDao;

    @Resource
    private RentBoothsDao rent_boothsDao;
    
    @Resource
    private PeopleDao peopleDao;

    //添加摊位
    @Override
    public HashMap<Object, Object> addbooths(Map<String, RentBooths> map ) {
        HashMap<Object, Object> reluse = new HashMap();
        StringBuffer arr = new StringBuffer();
        RentBooths rentBooths = map.get("RentBooths");
        System.out.println(rentBooths);

        for(int i = 0; i < rentBooths.getRb_photo().size(); ++i) {
            String imageStr = rentBooths.getRb_photo().get(i);
            arr.append(Base64Util.GenerateImage(imageStr) + ",");
        }

        rentBooths.setRb_photo2(arr.toString());
        rent_boothsDao.addbooths(rentBooths);
        if (this.rent_boothsDao.returnss() > 0) {
            reluse.put("status", "ok");
        } else {
            reluse.put("message", "添加失败");
        }

        return reluse;
    }

//    返回数据库被操作条数
    @Override
    public int returnss() {
        return rent_boothsDao.returnss();
    }

    /**
     * 删除地摊
     * @param map
     * @return
     */

    @Override
    public HashMap<Object, Object> deletebooths(Map<String, String> map) {
        HashMap<Object, Object> reluse = new HashMap();
        this.rent_boothsDao.deletebooths(Integer.valueOf(map.get("rb_id")));
        if (this.rent_boothsDao.returnss() > 0) {

        String[] rb_photos = map.get("rb_photo").split("\\,");
        for (int i = 0; i < rb_photos.length; i++) {

            String img_path = new ImageConst().getImageAddress() + rb_photos[i];
            File file = new File(img_path);
            if (file.exists()) {//文件是否存在
                if (file.delete()) {//存在就删了，返回1
                    reluse.put("status", "ok");
                } else {
                    reluse.put("status", "error");
                    reluse.put("message", "删除失败");
                }
            } else {
                reluse.put("status", "error");
                reluse.put("message", "文件不存在");
            }
        }

        } else {
            reluse.put("status", "error");
            reluse.put("message", "删除失败");
        }

        System.out.println(reluse);
        return reluse;
    }

    /**
     * 未租地摊
     * @return
     */
    @Override
    public HashMap<Object, Object> Notbooths() {
        HashMap<Object, Object> reluse =new HashMap<>();
        System.out.println(rent_boothsDao.Notbooths(0));
        reluse.put("RentBooths",rent_boothsDao.Notbooths(0));
        return reluse;
    }


    @Override
    public HashMap<Object, Object> searchidbooths(Map<String, String> map) {
        HashMap<Object, Object> reluse =new HashMap<>();
        RentBooths rentBooths = new RentBooths();
        Integer rb_id =new Integer(map.get("rb_id"));
        rentBooths.setRb_id(rb_id);
        System.out.println(rent_boothsDao.searchidbooths(1));
        reluse.put("searchidbooths",rent_boothsDao.searchidbooths(rentBooths.getRb_id()));
        System.out.println(reluse);
        return reluse;
    }

    //    获取摊位状态
    @Override
    public int getrb_state(Integer rb_id) {
        return rent_boothsDao.getrb_state(rb_id);
    }

    @Override
    public void Updatastate(Integer rb_state, Integer rb_id) {

    }

    @Override
    public Map<String,Object> queryBooths() {
        Map<String,Object> booths = new HashMap<>();
        List<RentBooths2> rentBooths2s = rent_boothsDao.queryBooths();
        booths.put("anybooths",rentBooths2s);
        return booths;
    }


    //判断用户是否申报过摊位
    public HashMap<Object, Object> findTheBoots(Map<String,String> map)  {
        String phone = map.get("phone");

        HashMap res = new HashMap();
        RentBooths2 rentBooths2 = rent_boothsDao.searchBoothStatusByPhone(phone);
        if (rentBooths2!=null){
            res.put("status","error");
        }else {
            res.put("status","ok");
        }
        return  res;
    }
    //获取用户的摊位
    public List<LinkSuperVO> selectRentBootsByPhone(String phone){

        List<LinkSuperVO> rentBooths2 = rent_boothsDao.searchBoothByPhone(phone);
        System.out.println(rentBooths2);
        return  rentBooths2;
    }
    /**
     * 用户摊位更新
     * @return 返回
     */
    public boolean updateBooths(RentBooths rentBooth){
        StringBuffer arr = new StringBuffer();

        try {
            RentBooths2 rentBooths2 = new RentBooths2();
            if (rentBooth.getRb_photo() != null){
                RentBooths2 searchidbooths = rent_boothsDao.searchidbooths(rentBooth.getRb_id());
                String[] photolist = searchidbooths.getRb_photo().split("\\,");
                for (int i = 0; i <photolist.length ; i++) {
                    System.out.println(photolist[i]);
                    String img_path = new ImageConst().getImageAddress() + photolist[i];
                    File file = new File(img_path.toString());
                    if (file.exists()) {//文件是否存在
                        if (file.delete()) {//存在就删了，返回1
                            System.out.println("删除成功");
                        } else {
                            System.out.println("删除失败");

                        }
                    } else {
                        System.out.println("文件不存在");

                    }
                }
            }

            for(int i = 0; i < rentBooth.getRb_photo().size(); ++i) {
                String imageStr = rentBooth.getRb_photo().get(i);
                arr.append(Base64Util.GenerateImage(imageStr) + ",");
            }

            rentBooths2.setRb_id(rentBooth.getRb_id());
            rentBooths2.setRb_ad_id(rentBooth.getRb_ad_id());
            rentBooths2.setRb_photo(arr.toString());
            rentBooths2.setRb_address(rentBooth.getRb_address());
            rentBooths2.setRb_introduce(rentBooth.getRb_introduce());
            rentBooths2.setRb_latitude(rentBooth.getRb_latitude());
            rentBooths2.setRb_longitude(rentBooth.getRb_longitude());

            System.out.println("renboothss2::"+rentBooths2);

            rent_boothsDao.updateBooth(rentBooths2);
            return true;
        }catch (Exception e){
            return false;
        }
    }
    /**
     * 返回申报或申请的信息
     * 申请是link表 mer_state为0是查询所有 link表
     * 申报是the_booths表 mer_state为其他时查询所有申报的地摊,
     * 还有用户信息
     */
    public List getOnBooths(Integer mer_state){

        List<LinkSuperVO> linkSuperVOS;
        System.out.println(mer_state);
        if(mer_state == 0){
            //为0查询所有未审核的申请=
            linkSuperVOS = linkDao.findLinkNoView();
        }else{
            //查询所有未审核的申报
            linkSuperVOS = rent_boothsDao.getOnBooths();
        }
        return linkSuperVOS;
    }


    /**
     * 通过或拒绝用户申报的摊位
     * 传入申报摊位的id,与状态 0代表同意,4代表拒绝
     * @return
     */
    public boolean isbefore(int id,int a_static){
        return rent_boothsDao.isbefore(id,a_static);
    }

}
