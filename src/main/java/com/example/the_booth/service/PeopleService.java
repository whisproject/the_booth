package com.example.the_booth.service;

import com.example.the_booth.entity.People;
import org.apache.ibatis.annotations.Param;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 王福坤
 * 2020/8/3 10:16
 */

public interface PeopleService {
    /**
     * 新增数据
     */
    HashMap<Object,Object> insert(Map<String,People> map) throws UnsupportedEncodingException, NoSuchAlgorithmException;
    /**
     * 查询账号所有内容
     */
    People TestExists(String mer_id);
    /**
     * 登录
     */
    HashMap<Object,Object> login(Map<String,People> map) throws UnsupportedEncodingException, NoSuchAlgorithmException;
    /**
     * 查看密码
     */
    String QueryLogin(String mer_id);
    /**
     * 修改密码
     */
    HashMap<Object,Object> Modify(Map<String,Object> map) throws UnsupportedEncodingException, NoSuchAlgorithmException;
    /**
     * 数据库修改条数
     */
    int returns();


    /**
     * 所有管理员信息
     */
    HashMap<Object, Object> infadmin();

    /**
     * 所有用户信息
     */
    HashMap<Object, Object> infuser();

    /**
     * 店铺id查询人员名字
     */

    Map<String,String> queryName(Integer rb_id);

    /**
     * 删除用户
     * @param map
     * @return
     */
    Map<String,String> deleteUser(Map<String,String> map);
    List<People> findUserByNameAndPhone(String name,String phone);


}
