package com.example.the_booth.service;

import com.example.the_booth.entity.People;
import com.example.the_booth.entity.RentBooths;
import com.example.the_booth.entity.RentBooths2;
import com.example.the_booth.vo.LinkSuperVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 王福坤
 * 2020/8/5 14:55
 */
public interface RentBoothsService {
//    添加摊位
    HashMap<Object,Object> addbooths(Map<String, RentBooths> map);
//    返回数据库修改数据条数
    int returnss();
//    删除摊位
    HashMap<Object, Object> deletebooths(Map<String, String> map);
//    未租摊位
    HashMap<Object, Object> Notbooths();

    HashMap<Object, Object> searchidbooths(Map<String, String> map);

    //        获取摊位状态
    int getrb_state(Integer rb_id);

    //    更改摊位状态
    void Updatastate(Integer rb_state,Integer rb_id);

    Map<String,Object> queryBooths();
    HashMap<Object, Object> findTheBoots(Map<String,String> map);

    boolean updateBooths(RentBooths rentBooth);
    public List<LinkSuperVO> selectRentBootsByPhone(String phone);
        /**
         * 返回申报上的信息
         */
    List<Object> getOnBooths(Integer mer_state);

    boolean isbefore(int id,int a_static);

}
