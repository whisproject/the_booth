package com.example.the_booth.service;

import com.example.the_booth.entity.RentBooths2;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

/**
 * 王福坤
 * 2020/8/5 16:03
 */
public interface LinkServicce {
//    用户申请摊位
    HashMap<Object, Object> applybooths(Map<String,Object> rentBooths2) throws ParseException;
//返回行数
    int linkreturns();

    boolean isafter(int id,int a_static);

}
