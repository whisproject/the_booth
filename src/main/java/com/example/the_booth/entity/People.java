package com.example.the_booth.entity;

import java.util.Objects;

    public class People {
        private String mer_id;//id,用手机号作用商家id
        private String mer_photo;//用户头像
        private String mer_salt;//加密盐,md5加密
        private String mer_password;//密码
        private String mer_name;//姓名
        private String mer_identity;//身份证
        private String mer_role;//角色 a管理员,b商家,c超级管理员
        private Object mer_static;//角色的状态,如果为false代表角色不可用

        public People() {
        }

        public People(String mer_id) {
            this.mer_id = mer_id;
        }

        public People(String mer_id, String mer_photo, String mer_salt, String mer_password, String mer_name, String mer_identity, String mer_role, Object mer_static) {
            this.mer_id = mer_id;
            this.mer_photo = mer_photo;
            this.mer_salt = mer_salt;
            this.mer_password = mer_password;
            this.mer_name = mer_name;
            this.mer_identity = mer_identity;
            this.mer_role = mer_role;
            this.mer_static = mer_static;
        }

        public String getMer_id() {
            return mer_id;
        }

        public void setMer_id(String mer_id) {
            this.mer_id = mer_id;
        }

        public String getMer_photo() {
            return mer_photo;
        }

        public void setMer_photo(String mer_photo) {
            this.mer_photo = mer_photo;
        }

        public String getMer_salt() {
            return mer_salt;
        }

        public void setMer_salt(String mer_salt) {
            this.mer_salt = mer_salt;
        }

        public String getMer_password() {
            return mer_password;
        }

        public void setMer_password(String mer_password) {
            this.mer_password = mer_password;
        }

        public String getMer_name() {
            return mer_name;
        }

        public void setMer_name(String mer_name) {
            this.mer_name = mer_name;
        }

        public String getMer_identity() {
            return mer_identity;
        }

        public void setMer_identity(String mer_identity) {
            this.mer_identity = mer_identity;
        }

        public String getMer_role() {
            return mer_role;
        }

        public void setMer_role(String mer_role) {
            this.mer_role = mer_role;
        }

        public Object getMer_static() {
            return mer_static;
        }

        public void setMer_static(Object mer_static) {
            this.mer_static = mer_static;
        }

        @Override
        public String toString() {
            return "People{" +
                    "mer_id='" + mer_id + '\'' +
                    ", mer_photo='" + mer_photo + '\'' +
                    ", mer_salt='" + mer_salt + '\'' +
                    ", mer_password='" + mer_password + '\'' +
                    ", mer_name='" + mer_name + '\'' +
                    ", mer_identity='" + mer_identity + '\'' +
                    ", mer_role='" + mer_role + '\'' +
                    ", mer_static=" + mer_static +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            People people = (People) o;
            return Objects.equals(mer_id, people.mer_id) &&
                    Objects.equals(mer_photo, people.mer_photo) &&
                    Objects.equals(mer_salt, people.mer_salt) &&
                    Objects.equals(mer_password, people.mer_password) &&
                    Objects.equals(mer_name, people.mer_name) &&
                    Objects.equals(mer_identity, people.mer_identity) &&
                    Objects.equals(mer_role, people.mer_role) &&
                    Objects.equals(mer_static, people.mer_static);
        }

        @Override
        public int hashCode() {
            return Objects.hash(mer_id, mer_photo, mer_salt, mer_password, mer_name, mer_identity, mer_role, mer_static);
        }

    }