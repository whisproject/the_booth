package com.example.the_booth.entity;

/**
 * 王福坤
 * 2020/8/5 14:41
 */
public class Link {
    private Integer li_id;
    private String li_mer_id;
    private Integer li_rb_id;
    private String li_begin_time;
    private String li_over_time;
    private Double li_static;
    private String li_industry;
    private String li_introduce;



    public Link() {
    }

    public Link(Integer li_id, String li_mer_id, Integer li_rb_id, String li_begin_time, String li_over_time, Double li_static) {
        this.li_id = li_id;
        this.li_mer_id = li_mer_id;
        this.li_rb_id = li_rb_id;
        this.li_begin_time = li_begin_time;
        this.li_over_time = li_over_time;
        this.li_static = li_static;
    }

    @Override
    public String toString() {
        return "Link{" +
                "li_id=" + li_id +
                ", li_mer_id='" + li_mer_id + '\'' +
                ", li_rb_id=" + li_rb_id +
                ", li_begin_time=" + li_begin_time +
                ", li_over_time=" + li_over_time +
                ", li_static=" + li_static +
                '}';
    }

    public Integer getLi_id() {
        return li_id;
    }

    public void setLi_id(Integer li_id) {
        this.li_id = li_id;
    }

    public String getLi_mer_id() {
        return li_mer_id;
    }

    public void setLi_mer_id(String li_mer_id) {
        this.li_mer_id = li_mer_id;
    }

    public Integer getLi_rb_id() {
        return li_rb_id;
    }

    public void setLi_rb_id(Integer li_rb_id) {
        this.li_rb_id = li_rb_id;
    }

    public String getLi_begin_time() {
        return li_begin_time;
    }

    public void setLi_begin_time(String li_begin_time) {
        this.li_begin_time = li_begin_time;
    }

    public String getLi_over_time() {
        return li_over_time;
    }

    public void setLi_over_time(String li_over_time) {
        this.li_over_time = li_over_time;
    }

    public Double getLi_static() {
        return li_static;
    }

    public void setLi_static(Double li_static) {
        this.li_static = li_static;
    }
    public String getLi_industry() {
        return li_industry;
    }

    public void setLi_industry(String li_industry) {
        this.li_industry = li_industry;
    }

    public String getLi_introduce() {
        return li_introduce;
    }

    public void setLi_introduce(String li_introduce) {
        this.li_introduce = li_introduce;
    }
}
