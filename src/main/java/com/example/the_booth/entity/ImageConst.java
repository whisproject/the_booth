package com.example.the_booth.entity;

public class ImageConst {

    //图片保存地址
    private String imageAddress ="src/main/resources/imageData/";

    public String getImageAddress() {
        return imageAddress;
    }

    public void setImageAddress(String imageAddress) {
        this.imageAddress = imageAddress;
    }
}
