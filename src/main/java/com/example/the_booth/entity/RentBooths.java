package com.example.the_booth.entity;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Objects;

/**
 * 王福坤
 * 2020/8/5 13:13
 */
public class RentBooths {
    private Integer rb_id;
    private String rb_adm_id;
    private String rb_ad_id;
    private Integer rb_state;
    private List<String> rb_photo;
    private String rb_address;
    private String rb_introduce;
    private BigDecimal rb_longitude;
    private BigDecimal rb_latitude;
    private String rb_photo2;

    public RentBooths(String rb_photo2) {
        this.rb_photo2 = rb_photo2;
    }

    @Override
    public String toString() {
        return "RentBooths{" +
                "rb_id=" + rb_id +
                ", rb_adm_id='" + rb_adm_id + '\'' +
                ", rb_ad_id='" + rb_ad_id + '\'' +
                ", rb_state=" + rb_state +
                ", rb_photo=" + rb_photo +
                ", rb_address='" + rb_address + '\'' +
                ", rb_introduce='" + rb_introduce + '\'' +
                ", rb_longitude=" + rb_longitude +
                ", rb_latitude=" + rb_latitude +
                ", rb_photo2='" + rb_photo2 + '\'' +
                '}';
    }

    public RentBooths(Integer rb_id, String rb_adm_id, String rb_ad_id, Integer rb_state, List<String> rb_photo, String rb_address, String rb_introduce, BigDecimal rb_longitude, BigDecimal rb_latitude, String rb_photo2) {
        this.rb_id = rb_id;
        this.rb_adm_id = rb_adm_id;
        this.rb_ad_id = rb_ad_id;
        this.rb_state = rb_state;
        this.rb_photo = rb_photo;
        this.rb_address = rb_address;
        this.rb_introduce = rb_introduce;
        this.rb_longitude = rb_longitude;
        this.rb_latitude = rb_latitude;
        this.rb_photo2 = rb_photo2;
    }

    public RentBooths() {
    }

    public String getRb_photo2() {
        return rb_photo2;
    }

    public void setRb_photo2(String rb_photo2) {
        this.rb_photo2 = rb_photo2;
    }

    public Integer getRb_id() {
        return rb_id;
    }

    public void setRb_id(Integer rb_id) {
        this.rb_id = rb_id;
    }

    public String getRb_adm_id() {
        return rb_adm_id;
    }

    public void setRb_adm_id(String rb_adm_id) {
        this.rb_adm_id = rb_adm_id;
    }

    public String getRb_ad_id() {
        return rb_ad_id;
    }

    public void setRb_ad_id(String rb_ad_id) {
        this.rb_ad_id = rb_ad_id;
    }

    public Integer getRb_state() {
        return rb_state;
    }

    public void setRb_state(Integer rb_state) {
        this.rb_state = rb_state;
    }

    public List<String> getRb_photo() {
        return rb_photo;
    }

    public void setRb_photo(List<String> rb_photo) {
        this.rb_photo = rb_photo;
    }

    public String getRb_address() {
        return rb_address;
    }

    public void setRb_address(String rb_address) {
        this.rb_address = rb_address;
    }

    public String getRb_introduce() {
        return rb_introduce;
    }

    public void setRb_introduce(String rb_introduce) {
        this.rb_introduce = rb_introduce;
    }

    public BigDecimal getRb_longitude() {
        return rb_longitude;
    }

    public void setRb_longitude(BigDecimal rb_longitude) {
        this.rb_longitude = rb_longitude;
    }

    public BigDecimal getRb_latitude() {
        return rb_latitude;
    }

    public void setRb_latitude(BigDecimal rb_latitude) {
        this.rb_latitude = rb_latitude;
    }
}
