package com.example.the_booth.dao;

import com.example.the_booth.entity.Link;
import com.example.the_booth.vo.LinkSuperVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 王福坤
 * 2020/8/5 16:02
 */
@Mapper
public interface LinkDao {

    void applybooths(Link link);

    int linkreturns();

    String queryphone(String li_mer_id);

    String rbidQueryphone(Integer li_rb_id);

    Integer queryRbid(String li_mer_id);

    boolean isafter(@Param("id") int id, @Param("a_static") int a_static);
    //查看没有审核的请求
    List<LinkSuperVO> findLinkNoView();

    String liidQueryphone(Integer li_id);

}
