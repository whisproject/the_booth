package com.example.the_booth.dao;

import com.example.the_booth.entity.RentBooths;
import com.example.the_booth.entity.RentBooths2;
import com.example.the_booth.vo.LinkSuperVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 王福坤
 * 2020/8/5 14:54
 */
@Mapper
public interface RentBoothsDao {
    void addbooths(RentBooths rent_booths);
    int returnss();
    void deletebooths(Integer mer_id);
    List<RentBooths> Notbooths(Integer rb_state);
    RentBooths2 searchidbooths(Integer rb_id);
    //    获取摊位状态
    int getrb_state(Integer rb_id);

    //      修改摊位状态
    void Updatastate(Integer rb_state,Integer rb_id);

    //查询所有摊位
    List<RentBooths2> queryBooths();

    int updateBooth(RentBooths2 rentBooths);

    Integer searchBoothStatus(Integer rb_id);
    //通过手机号或胜名查找所拥有的地摊
    RentBooths2 searchBoothStatusByPhone(@Param("phone") String phone);
    //通过手机号查询地滩

    List<LinkSuperVO> searchBoothByPhone(@Param("phone") String phone);
    //返回正在申报或正在申请的摊位信息
    List<LinkSuperVO> getOnBooths();

    boolean isbefore(@Param("id") int id, @Param("a_static") int a_static);
}
