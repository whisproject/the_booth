package com.example.the_booth.dao;

import com.example.the_booth.entity.People;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 王福坤
 * 2020/8/3 10:19
 */
@Mapper
public interface PeopleDao {
    void insert(People people);
    People TestExists(String mer_id);
    String QueryLogin(String mer_id);
    void Updatapassword(String mer_password, String mer_id);
    int returns();
    List<People> infpeople(String mer_role);
    String queryName(String mer_id);
    int deleteUser(String mer_id);
    //增加用户
    void insertUser(People people);
    //通过手机号或姓名查看用户,用户为模糊查询
    List<People> findUserByNameAndPhone(@Param("name") String name,@Param("phone") String phone);
}
