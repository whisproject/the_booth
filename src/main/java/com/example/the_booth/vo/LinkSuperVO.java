package com.example.the_booth.vo;

import com.example.the_booth.entity.Link;
import com.example.the_booth.entity.People;
import com.example.the_booth.entity.RentBooths2;
/*
* 此类是link的超级级*/
public class LinkSuperVO {
    private People people;
    private RentBooths2 rentBooths2;
    private Link link;

    @Override
    public String toString() {
        return "LinkSuperVO{" +
                "people=" + people +
                ", rentBooths2=" + rentBooths2 +
                ", link=" + link +
                '}';
    }

    public People getPeople() {
        return people;
    }

    public void setPeople(People people) {
        this.people = people;
    }

    public RentBooths2 getRentBooths2() {
        return rentBooths2;
    }

    public void setRentBooths2(RentBooths2 rentBooths2) {
        this.rentBooths2 = rentBooths2;
    }

    public Link getLink() {
        return link;
    }

    public void setLink(Link link) {
        this.link = link;
    }
}
