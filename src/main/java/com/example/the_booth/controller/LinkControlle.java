package com.example.the_booth.controller;

import com.example.the_booth.entity.RentBooths2;
import com.example.the_booth.service.LinkServicce;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

/**
 * 王福坤
 * 2020/8/6 21:03
 */
@RestController
@RequestMapping("link")
public class LinkControlle {
    @Resource
    private LinkServicce linkServicce;

    /**
     * 申请摊位
     */
    @RequestMapping(value = "applybooths")
    public HashMap<Object, Object> applybooths(@RequestBody Map<String,Object> rentBooths2) throws ParseException {
        System.out.println(rentBooths2);
        return  linkServicce.applybooths(rentBooths2);
    }

    /**
     * 每一条申请在link表都有记录,static字段代表这条记录是否可用
     * 通过或拒绝用户申请的摊位  传入申请摊位的id,与状态 1 代表允许,2代表拒绝
     * 同意时同时要在rentboots表修改摊位状态为1(已租)
     * @return
     */
    @RequestMapping(value = "isafter")
    public HashMap<String,Boolean> isafter(@RequestBody Map<String,Integer> map){
        //通过id为109的记录,1代表同意,此值由前端传递
        boolean isafter = linkServicce.isafter(map.get("li_id"), map.get("a_static"));
        HashMap<String,Boolean> relst = new HashMap<>();
        if (isafter){
            relst.put("isafter",isafter);
        }else {
            relst.put("isafter",false);
        }
        return relst;
    }
}
