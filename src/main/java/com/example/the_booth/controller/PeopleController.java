package com.example.the_booth.controller;

import com.example.the_booth.entity.People;
import com.example.the_booth.entity.RentBooths2;
import com.example.the_booth.service.PeopleService;
import com.example.the_booth.util.MD5Util;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 王福坤
 * 2020/8/3 10:21
 */

@RestController
@RequestMapping("people")
public class PeopleController {
//    服务对象
    @Resource
    private PeopleService peopleService;

    /**
     * 注册
     */
    @RequestMapping(value = "insertPeople")
    public HashMap<Object, Object> insert(@RequestBody Map<String,People> map) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        return peopleService.insert(map);

    }

    /**
     * 登录
//     */
    @RequestMapping(value = "loginpeople")
    public HashMap<Object, Object> login(@RequestBody Map<String,People> map) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        System.out.println(map);
        return  peopleService.login(map);
    }



    /**
     * 修改密码
     */
    @RequestMapping(value = "Modifypeople")
    public HashMap<Object, Object> Modify(@RequestBody Map<String,Object> map) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        return  peopleService.Modify(map);
    }

    /**
     * 管理员账号信息
     */
    @RequestMapping(value = "infadmin")
    public HashMap<Object, Object> infadmin(){
        return  peopleService.infadmin();
    }


    /**
     * 用户账号信息
     */
    @RequestMapping(value = "infuser")
    public HashMap<Object, Object> infuser(){
        return  peopleService.infuser();
    }

    /**
     * 通过摊位id寻找摊主的名字
     * @return
     */
    @RequestMapping("queryname")
    public Map<String,String> queryName(@RequestBody Map<String,String> map){
        return peopleService.queryName(Integer.valueOf(map.get("rb_id")));
    }

    /**
     * 删除用户
     * @param map
     * @return
     */
    @RequestMapping("deleteuser")
    public Map<String,String> deleteuser(@RequestBody Map<String,String> map){
        return peopleService.deleteUser(map);
    }
//    List<People> findUserByNameAndPhone(String name, String phone);


    //通过手机或姓名获取用户,如果为空传入null即可,条件关联为or
    @RequestMapping("findUser")
    public Map<String,List<People>> findUserByNameAndPhone(@RequestBody Map<String,String> map){

        HashMap<String,List<People>> peoplelist  = new HashMap<>();
        List<People> name_search = peopleService.findUserByNameAndPhone(map.get("search"), null);
            List<People> phone_search = peopleService.findUserByNameAndPhone(null, map.get("search"));

            if (name_search.isEmpty()){
                peoplelist.put("peoplelist",phone_search);
            }
            if (phone_search.isEmpty()){
                peoplelist.put("peoplelist",name_search);
            }

        System.out.println(peoplelist);

        return peoplelist;
    }



}
