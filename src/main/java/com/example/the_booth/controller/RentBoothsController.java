package com.example.the_booth.controller;

import com.example.the_booth.entity.RentBooths;
import com.example.the_booth.entity.RentBooths2;
import com.example.the_booth.service.RentBoothsService;
import com.example.the_booth.vo.LinkSuperVO;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 王福坤
 * 2020/8/5 15:01
 */
@RestController
@RequestMapping("booths")
public class RentBoothsController {
    //    服务对象
    @Resource
    private RentBoothsService rent_boothsService;

    /**
     * 添加摊位信息
     */
    @RequestMapping(value = "addbooths")
    public HashMap<Object, Object> addbooths(@RequestBody Map<String, RentBooths> map)  {
        return rent_boothsService.addbooths(map);
    }

    /**
     * 删除摊位
     */
    @RequestMapping(value = "deletebooths")
    @ResponseBody
    public HashMap<Object, Object> deletebooths(@RequestBody Map<String,String> map)  {
        return rent_boothsService.deletebooths(map);
    }


    /**
     * 未租摊位
     */
    @RequestMapping(value = "notbooths")
    public HashMap<Object, Object> notbooths()  {
        return rent_boothsService.Notbooths();
    }


    @RequestMapping(value = "searchidbooths")
    public HashMap<Object, Object>  searchidbooths(@RequestBody Map<String,String> map)  {
        return rent_boothsService. searchidbooths(map);
    }

    @RequestMapping("anybooths")
    public Map<String,Object> anyBooths(){
        return rent_boothsService.queryBooths();
    }

    /**
     * 通过手机号或姓名搜索地摊状态
     */
    @RequestMapping(value = "findTheBoots")
    public HashMap<Object, Object> findTheBoots(@RequestBody Map<String,String> map)  {
        System.out.println(map);
        return rent_boothsService.findTheBoots(map);
    }
    @RequestMapping(value = "findTheBootsByPhone")
    public HashMap<Object, Object> findTheBootsByPhone(@RequestBody Map<String,String> map)  {
        HashMap<Object, Object> res = new HashMap<>();
        System.out.println(map.get("phone"));
        List<LinkSuperVO> lsv =  rent_boothsService.selectRentBootsByPhone(map.get("phone"));
//        System.out.println(lsv);
        if (lsv.isEmpty()){
            res.put("status","error");
            res.put("message","该用户没有租借摊位");

        }else {
            if (lsv.get(0).getRentBooths2() == null){
                res.put("status","error");
                res.put("message","该用户没有租借摊位");
            }else {
                res.put("status","ok");
                res.put("lsv",lsv);
            }
        }

        return res;
    }

    /**
     * 用户摊位更新,更新成功返回true,失败返回false
     */
    @RequestMapping(value = "updateBooths")
    public Map<String,String> updateBooths(@RequestBody Map<String,RentBooths> map){
        HashMap<String,String> relse = new HashMap<>();
        System.out.println(map);
        RentBooths rentbooth = map.get("rentbooth");

        boolean b = rent_boothsService.updateBooths(rentbooth);
        if (b){
            relse.put("status","ok");
            relse.put("message","更新地摊信息成功!");
        }else {
            relse.put("status","error");
            relse.put("message","更新地摊信息失败!");
        }

        return relse;
    }


    /**
     * 返回申报或申请的信息
     * 申请是link表 mer_state为0是查询所有 link表
     * 申报是the_booths表 mer_state为其他时查询所有申报的地摊,
     * 还有用户信息
     */
    @RequestMapping("applyBooths")
    public Map<String,Object> getOnBooths(@RequestBody Map<String,String> map){
        int mer_status = Integer.parseInt(map.get("mer_status"));

        System.out.println(mer_status);
        List list = rent_boothsService.getOnBooths(mer_status);
        Map res = new HashMap<>();
        res.put("res",list);
        return res;
    }

    /**
     * 通过或拒绝用户申报的摊位
     * 传入申报摊位的id,与状态 0代表同意,4代表拒绝
     * @return
     */
    @RequestMapping(value = "isbefore")
    public Map<String,String> isbefore(@RequestBody Map<String,RentBooths> map){
        //将id为1的地摊状态设置为4,也就是拒绝
        rent_boothsService.isbefore(1,4);
        return null;
    }
}
