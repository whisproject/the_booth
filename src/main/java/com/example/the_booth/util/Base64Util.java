package com.example.the_booth.util;

import com.example.the_booth.entity.ImageConst;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;
import sun.misc.Contended;

import java.io.*;

/**
 * 王福坤
 * 2020/8/6 10:41
 */
public class Base64Util {

    // 图片转化成base64字符串
    public static String GetImageStr() {
        String imgFile = "D:\\tanbing.jpg";// 待处理的图片
        InputStream in = null;
        byte[] data = null;
        // 读取图片字节数组
        try {
            in = new FileInputStream(imgFile);
            data = new byte[in.available()];
            in.read(data);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 对字节数组Base64编码
        BASE64Encoder encoder = new BASE64Encoder();
        System.out.println(encoder.encode(data));
        return encoder.encode(data);// 返回Base64编码过的字节数组字符串
    }

    // 对字节数组字符串进行Base64解码并生成图片
    public static String GenerateImage(String imgStr) {

        if (imgStr == null) {
            // 图像数据为空
            return "图像数据为空";
        }
        BASE64Decoder decoder = new BASE64Decoder();
        try {
            // Base64解码
            byte[] b = decoder.decodeBuffer(imgStr);
            for (int i = 0; i < b.length; ++i) {
                if (b[i] < 0) {// 调整异常数据
                    b[i] += 256;
                }
            }
            // 生成jpeg图片
            String imgname =RandomDutil.genRandomNum()+".jpg";
            String imgFilePath = new ImageConst().getImageAddress() +imgname;// 新生成的图片
            OutputStream out = new FileOutputStream(imgFilePath);
            out.write(b);
            out.flush();
            out.close();
            return imgname;
        } catch (Exception e) {
            return "错误";
        }
    }
}
