package com.example.the_booth.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
    //新增加一个类用来添加虚拟路径映射
    @Configuration
    public class MyImageConfig implements WebMvcConfigurer {

        @Value("${image.address}")
        public String address;

        @Override
        public void addResourceHandlers(ResourceHandlerRegistry registry) {

            registry.addResourceHandler("/image/**")
            .addResourceLocations("classpath:/imageData/")
                    .addResourceLocations("file:"+address);

            System.out.println("加载路径配置");
        }
    }
